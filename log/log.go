package log

import (
	"bitbucket.org/taringa/imageid/config"
	"errors"
	"log"
)

var debug bool

func init() {
	debug = config.Env("IMAGEID_DEBUG", "") != ""
}

func Debugf(format string, args ...interface{}) {
	if debug {
		log.Printf("[DEBUG] "+format, args...)
	}
}

func Errorf(format string, args ...interface{}) {
	log.Printf("[ERROR] "+format, args...)
}

func Infof(format string, args ...interface{}) {
	log.Printf("[INFO] "+format, args...)
}

func OnErrorPanic(e error) {
	if e != nil {
		panic(e)
	}
}

func OnErrorLog(from string, e error) bool {
	if e != nil {
		Errorf("[%s] %v", from, e)
		return true
	}
	return false
}

func toError(e interface{}) error {
	switch x := e.(type) {
	case string:
		return errors.New(x)
	case error:
		return x
	default:
		return errors.New("Unknown panic")
	}
}

func OnPanicLog(from string) {
	e := recover()
	if e != nil {
		Errorf("[%s] %s", from, toError(e))
	}
}
