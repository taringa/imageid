package imageid

import (
	"bitbucket.org/taringa/imageid/kvstore"
	"bitbucket.org/taringa/imageid/log"
	"bitbucket.org/taringa/imageid/stats"
	"crypto/md5"
	"fmt"
	"io"
	"os"
)

func keyFromMd5(md5 []byte) string {
	return fmt.Sprintf("%032x", md5)
}

func computeMd5(filePath string) string {
	file, err := os.Open(filePath)
	log.OnErrorPanic(err)
	defer file.Close()

	hash := md5.New()
	_, err = io.Copy(hash, file)
	log.OnErrorPanic(err)

	return keyFromMd5(hash.Sum(nil))
}

func (db *DB) addMd5(tx kvstore.Tx, md5 string, url string) error {
	stats.Md5Added(1)
	return db.Store.Add(tx, TableMD5s, md5, url)
}

func (db *DB) getCanonicalForMd5(md5 string) string {
	url, err := db.Store.Get(TableMD5s, md5)
	log.OnErrorPanic(err)
	return url
}

func (db *DB) queryMd5(filename string) (string, string) {
	md5 := computeMd5(filename)
	return md5, db.getCanonicalForMd5(md5)
}
