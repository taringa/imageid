package mtree

import (
	"bitbucket.org/taringa/imageid/imhash"
	"github.com/stretchr/testify/assert"
	"math/rand"
	"testing"
)

func ImageHash(dhash, phash uint64) imhash.ImageHash {
	return imhash.ImageHash{Dhash: dhash, Phash: phash}
}

func TestEmpty(t *testing.T) {
	m := New()
	assert.NotNil(t, m)
	found, _, _, nodesVisited := m.SearchClosest(imhash.ImageHash{}, 5)
	assert.Equal(t, false, found)
	assert.Equal(t, 0, nodesVisited)
}

func assertFound(t *testing.T, m *MTree, hash imhash.ImageHash, d int, expectedClosest imhash.ImageHash, expectedDist int, expectedNodesVisited int64) {
	found, closestHash, dist, nodesVisited := m.SearchClosest(hash, d)
	assert.True(t, found, "%v", hash)
	assert.Equal(t, expectedClosest, closestHash)
	assert.Equal(t, expectedDist, dist)
	assert.Equal(t, expectedNodesVisited, nodesVisited)
}

func assertNotFound(t *testing.T, m *MTree, hash imhash.ImageHash, d int, expectedNodesVisited int64) {
	found, _, _, nodesVisited := m.SearchClosest(hash, d)
	assert.False(t, found)
	assert.Equal(t, expectedNodesVisited, nodesVisited)
}

func TestOneHash(t *testing.T) {
	m := New()

	hash := ImageHash(1, 0)
	depth := m.Add(hash)
	assert.Equal(t, 1, depth)

	assertFound(t, m, ImageHash(1, 0), 5, hash, 0, 1)
	assertNotFound(t, m, ImageHash(0, 0), 0, 1)
	assertFound(t, m, ImageHash(0, 0), 1, hash, 1, 1)
	assertFound(t, m, ImageHash(1, 0), 0, hash, 0, 1)
}

func TestTwoHashes(t *testing.T) {
	m := New()

	hash1 := ImageHash(1, 0)
	depth := m.Add(hash1)
	assert.Equal(t, 1, depth)

	hash2 := ImageHash(2, 0)
	depth = m.Add(hash2)
	assert.Equal(t, 1, depth)

	assertFound(t, m, ImageHash(1, 0), 5, hash1, 0, 1)
	assertFound(t, m, ImageHash(2, 0), 5, hash2, 0, 2)
	assertNotFound(t, m, ImageHash(0, 0), 0, 2)
	assertFound(t, m, ImageHash(0, 0), 1, hash1, 1, 2)
	assertFound(t, m, ImageHash(6, 0), 1, hash2, 1, 2)
	assertFound(t, m, ImageHash(1, 0), 0, hash1, 0, 1)
	assertFound(t, m, ImageHash(2, 0), 0, hash2, 0, 2)
}

func TestBeforeSplit(t *testing.T) {
	m := New()

	for i := uint(0); i < 16; i++ {
		depth := m.Add(ImageHash(1<<i, 0))
		assert.Equal(t, 1, depth)
	}

	assertFound(t, m, ImageHash(1, 0), 5, ImageHash(1, 0), 0, 1)
	assertFound(t, m, ImageHash(1<<15, 0), 5, ImageHash(1<<15, 0), 0, 16)
	assertNotFound(t, m, ImageHash(1<<16, 0), 0, 16)
}

func ones(i uint) imhash.ImageHash { return ImageHash((1<<i)-1, 0) }

func TestSplit(t *testing.T) {
	m := New()

	for i := uint(0); i < 16; i++ {
		depth := m.Add(ones(i))
		assert.Equal(t, 1, depth)
	}
	depth := m.Add(ones(16))
	assert.Equal(t, 2, depth)

	fork := m.data.(*forkNode)
	near := fork.near.data.(*leafNode)
	far := fork.far.data.(*leafNode)

	assert.Equal(t, imhash.ImageHash{}, fork.hash)
	assert.Equal(t, 8, near.amount)
	assert.Equal(t, 8, fork.nearDist)
	assert.Equal(t, 8, near.hashes[7].Distance(fork.hash))
	assert.Equal(t, 9, far.hashes[0].Distance(fork.hash))
	assert.Equal(t, 8, far.amount)

	assertFound(t, m, ones(0), 0, ones(0), 0, 1)
	assertNotFound(t, m, ImageHash(2, 0), 0, 9)
	assertFound(t, m, ones(1), 0, ones(1), 0, 2)
	assertFound(t, m, ones(16), 0, ones(16), 0, 9)
}

func randomHash() imhash.ImageHash {
	return ImageHash(
		uint64(rand.Int63()),
		uint64(rand.Int63()),
	)
}

func bench(b *testing.B, amount int) {
	for i := 0; i < b.N; i++ {
		m := New()
		for j := 0; j < amount; j++ {
			m.Add(randomHash())
		}
		for j := 0; j < amount; j++ {
			m.SearchClosest(randomHash(), 8)
		}
	}
}
func BenchmarkMTree1(b *testing.B)     { bench(b, 1) }
func BenchmarkMTree10(b *testing.B)    { bench(b, 10) }
func BenchmarkMTree100(b *testing.B)   { bench(b, 100) }
func BenchmarkMTree1000(b *testing.B)  { bench(b, 1000) }
func BenchmarkMTree10000(b *testing.B) { bench(b, 10000) }
