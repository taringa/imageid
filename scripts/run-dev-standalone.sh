set -ex

docker build -t imageid .

docker run -t -i --rm=true \
	--name imageid \
	-p 8080:8080 \
	-e 'IMAGEID_NULL_STORE=1' \
	-e 'IMAGEID_DEBUG=1' \
	-e 'IMAGEID_PROCESS_WORKERS=1' \
	-e 'IMAGEID_DOWNLOAD_WORKERS=1' \
	imageid \
	"$@"

