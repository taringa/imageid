set -ex

docker build -t imageid .

if [[ -z $(docker ps | grep 'mysql') ]]; then
	docker run --name mysql \
		-p 3306:3306 \
		-e 'MYSQL_ROOT_PASSWORD=root' \
		-e 'MYSQL_USER=imageid' \
		-e 'MYSQL_PASSWORD=imageid' \
		-e 'MYSQL_DATABASE=imageid' \
		-d mariadb
	sleep 15
fi

docker run -t -i --rm=true \
	--link mysql:mysql \
	--name imageid \
	-p 8080:8080 \
	-e 'IMAGEID_DEBUG=1' \
	-e 'IMAGEID_PROCESS_WORKERS=1' \
	-e 'IMAGEID_DOWNLOAD_WORKERS=1' \
	imageid \
	"$@"

